import React, { useState, useRef, forwardRef, useImperativeHandle } from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import DatePicker from 'react-native-date-picker';
import { useTheme } from '@react-navigation/native';

const formatTime = (date) => {
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');

    return hours + ":" + minutes;
}

var date = new Date();

const TPick = forwardRef((props,ref) => {
  const [date, setDate] = useState(new Date("1970-01-01T09:00:00.000Z"))
  const [open, setOpen] = useState(false)
  const { colors } = useTheme();

  useImperativeHandle(ref, () => ({ setDate }), [ ])

  const styles = StyleSheet.create({
    button: {
      backgroundColor: colors.border,
      height: 30,
    },
    buttonText: {
      fontSize: 20,
      textAlign: "center",
      color: colors.text,
    }
  });

  return (
    <>
      <TouchableOpacity onPress={() => setOpen(true)}  style={ styles.button }>
        <Text style={ styles.buttonText }>Reverse at: {formatTime(date)}</Text>
      </TouchableOpacity>
      <DatePicker
        textColor={colors.text}
        modal
        mode={"time"}
        open={open}
        date={date}
        onConfirm={(date) => {
          setOpen(false)
          setDate(date)
          props.getTime(date)
        }}
        onCancel={() => {
          setOpen(false)
        }}
      />
    </>
  )
});

export default TPick;

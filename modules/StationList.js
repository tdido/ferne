import React, { useState, useRef, forwardRef, useImperativeHandle} from "react"
import { Text, View } from "react-native"
import { AutocompleteDropdown } from "react-native-autocomplete-dropdown"
import { useTheme } from '@react-navigation/native';

import stations from '../res/stations.json';

const StationList = forwardRef((props,ref) => {
  const { colors } = useTheme();

  const [selectedItem, setSelectedItem] = useState(null)

  const stationListController = useRef(null)

  useImperativeHandle(ref, () => ({
    stationListController: stationListController
  }));

  return (
      <AutocompleteDropdown
        closeOnBlur={false}
        showClear={false}
        onSelectItem={(item) => {props.getStation(item) && setSelectedItem(item.id)}}
        dataSet={stations}
        controller={(controller) => {
          stationListController.current = controller
        }}
        textInputProps={{
          placeholder:props.placeholder,
          style: {
            backgroundColor: colors.card,
            color: colors.text,
          }
        }}
        suggestionsListContainerStyle={{
          backgroundColor: colors.card
        }}
        renderItem={(item, text) => (
          <Text style={{ color: colors.text, padding: 15 }}>{item.title}</Text>
        )}
        rightButtonsContainerStyle={{
          backgroundColor: colors.card
        }}
      />
  )
});

export default StationList;

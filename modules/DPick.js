import React, {useEffect, useState, useRef, forwardRef, useImperativeHandle} from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';
import DatePicker from 'react-native-date-picker';
import { useTheme } from '@react-navigation/native';

Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

const formatDate2 = (date) => {
    const month = date.toLocaleString('default', {  month: 'short' });
    const day = date.toLocaleString('default', {  weekday: 'short' }).split(",")[0];
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');

    return day + " " + date.getDate() + " " + month + " " + hours + ":" + minutes;
}

var date = new Date();

const maxDate = date.addDays(20);
const minDate = new Date();
minDate.setHours(0,0,0);
const currDate = new Date();
currDate.setMinutes(0);


const DPick = forwardRef((props,ref) => {
  const [date, setDate] = useState(currDate)
  const [open, setOpen] = useState(false)
  const { colors } = useTheme();

  useImperativeHandle(ref, () => ({ setDate }), [ ])

  const styles = StyleSheet.create({
    button: {
      backgroundColor: colors.border,
      padding: 5
    },
    buttonText: {
      fontSize: 20,
      textAlign: "center",
      color: colors.text,
    }
  });

  return (
    <>
      <TouchableOpacity onPress={() => setOpen(true)}  style={ styles.button }>
        <Text style={ styles.buttonText }>{formatDate2(date)}</Text>
      </TouchableOpacity>
      <DatePicker
        textColor={colors.text}
        modal
        open={open}
        date={date}
        minuteInterval={60}
        minimumDate={minDate}
        maximumDate={maxDate}
        onConfirm={(date) => {
          setOpen(false)
          setDate(date)
          props.getDate(date)
        }}
        onCancel={() => {
          setOpen(false)
        }}
      />
    </>
  )
});


export default DPick;

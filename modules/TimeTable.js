import React, {useEffect, useState, useRef} from 'react';
import {
  RefreshControl,
  Switch,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useTheme } from '@react-navigation/native';

import stations from '../res/stations.json';

var stationIdToName = stations.reduce(function(map, obj) {
    map[obj.id] = obj.title;
    return map;
}, {});

const formatDate = (date) => {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');

    return year+month+day;
}

const TimeTable = (props) =>  {
  const fecha = formatDate(props.fecha);
  const hours = String(props.fecha.getHours()).padStart(2, '0');
  const { colors } = useTheme();

  const styles = StyleSheet.create({
    text: {
      fontSize: 14,
      fontWeight: '500',
      paddingTop: 10,
      paddingBottom: 10,
      paddingLeft: 5,
      paddingRight: 5,
      textAlign:"center",
      textAlignVertical: "center"
    },
    row: {
      padding: 0,
      margin:2,
      flexDirection: "row",
    },
    header: {
      backgroundColor: colors.border,
    },
    headerText: {
      color: colors.text,
    },
    train: {
      backgroundColor: colors.background,
    },
    connections: {
      backgroundColor: colors.border,
      margin:2,
      color: colors.text,
    },
  });

  const payload = {
    "nucleo":"10",
    "origen":props.origen,
    "destino":props.destino,
    "fchaViaje":fecha,
    "validaReglaNegocio":true,
    "tiempoReal":true,
    "servicioHorarios":"VTI",
    "horaViajeOrigen":hours,
    "horaViajeLlegada":"24",
    "accesibilidadTrenes":true
  };

 const  requestOptions = {
   method: 'POST',
   headers: { 'Content-Type': 'application/json' },
   body: JSON.stringify(payload)
 };

 const [status, setStatus] = useState('');

 const [results, setResults] = useState([]);

 const fetchTimes = () => {
    setRefreshing(true);
    fetch('https://horarios.renfe.com/cer/HorariosServlet',requestOptions)
      .then(response => response.json())
      .then(setResults)
      .then(()=>{
          setStatus('Success');
          setRefreshing(false);
      })
      .catch(()=>setStatus('Error'));
 }

 useEffect(()=>{
    console.log("refreshing because of props update");
    fetchTimes();
  }, [props]);

  const [state,setState] = useState({activeSections: []});

  toggleExpanded = () => {
    setState({ collapsed: !state.collapsed });
  };

  setSections = (sections) => {
    setState({
      activeSections: sections.includes(undefined) ? [] : sections,
    });
  };

  renderHeader = (tren, _, isActive) => {
    const changes = tren.hasOwnProperty("trans") ? tren.trans.length : 0;
    
    const wheelchair = tren.accesible ? <Icon name="wheelchair" size={20} color="#900" /> : <></>;

    return (
      <View style={[styles.train, styles.row]}>
        <Text style={[styles.text,{flex:2}]}>
            {tren.linea}
        </Text>
        <Text style={[styles.text,{flex:2}]}>
            {tren.horaSalida}
        </Text>
        <Text style={[styles.text,{flex:2}]}>
            {tren.horaLlegada}
        </Text>
        <Text style={[styles.text,{flex:4}]}>
            {tren.duracion}
        </Text>
        <Text style={[styles.text,{flex:3}]}>
            {changes}
        </Text>
        <Text style={[styles.text,{flex:1}]}>
            {wheelchair} 
        </Text>
      </View>
    );
  };

  const renderContent = (tren, _, isActive) => {
    if(tren.hasOwnProperty("trans")){
      return tren.trans.map((obj, index) => {
        return (
          <Text style={styles.connections} key={index}>Change in {stationIdToName[obj.cdgoEstacion]} to {obj.linea} departing at {obj.horaSalida}</Text>
        );
      });
    }
  }
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
      setRefreshing(true);
      console.log("refreshing via pull");
      props.resetDate();
  }, []);

  switch(status){
    case "Error":
      return <Text>Error fetching data</Text>;
      break;
    case "Success":
      if(results){
          results.horario.sort(
            (a, b) => {
                const sa = a.horaSalida.split(':');
                const sb = b.horaSalida.split(':');
                const msa = parseInt(sa[0]) * 60 + parseInt(sa[1]);
                const msb = parseInt(sb[0]) * 60 + parseInt(sb[1]);
                const la = a.horaLlegada.split(':');
                const lb = b.horaLlegada.split(':');
                const mla = parseInt(la[0]) * 60 + parseInt(la[1]);
                const mlb = parseInt(lb[0]) * 60 + parseInt(lb[1]);

                return msa - msb || mla - mlb;
            }
          );
          return (
            <View style={{flex:1}}>
              <View style={[styles.header, styles.row]}>
                <Text style={[styles.headerText, styles.text, {flex:2}]}>
                    line
                </Text>
                <Text style={[styles.headerText, styles.text, {flex:2}]}>
                    dep.
                </Text>
                <Text style={[styles.headerText, styles.text, {flex:2}]}>
                    arr.
                </Text>
                <Text style={[styles.headerText, styles.text, {flex:4}]}>
                    duration
                </Text>
                <Text style={[styles.headerText, styles.text, {flex:3}]}>
                    changes
                </Text>
                <Text style={[styles.headerText, styles.text, {flex:1}]}>
                </Text>
              </View>
              <Accordion
                activeSections={state.activeSections}
                sections={results.horario}
                touchableComponent={TouchableOpacity}
                expandMultiple={true}
                renderHeader={renderHeader}
                renderContent={renderContent}
                duration={10}
                renderChildrenCollapsed={false}
                onChange={setSections}
                renderAsFlatList={true}
                containerStyle={{flex: 1}}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
              />
            </View>
          );
      }else{
        return <Text>No results for the selected options</Text>;
      }
      break;
    default:
      return <></>;
  }
}

export default TimeTable;

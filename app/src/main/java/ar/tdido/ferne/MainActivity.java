package ar.tdido.ferne;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    TimesAdapter timesAdapter;
    Spinner spFrom;
    Spinner spTo;
    Button btnSwitch;

    int cham = 14;
    int valde = 86;

    private void updateTimes(String payload) {
        JsonRequest jr = new JsonRequest();
        jr.setPayload(payload);
        jr.setUrl("https://horarios.renfe.com/cer/HorariosServlet");
        Thread thread = new Thread(jr);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String value = jr.getValue();
        String time;
        try {
            JSONObject data = new JSONObject(value);
            JSONArray times = data.getJSONArray("horario");
            ArrayList<String> timesList = new ArrayList<>();

            for (int i = 0; i < times.length(); i++) {
                time = times.getJSONObject(i).getString("linea");
                time += " " + times.getJSONObject(i).getString("horaSalida");
                time += " " + times.getJSONObject(i).getString("horaLlegada");
                time += " " + times.getJSONObject(i).getString("duracion");
                timesList.add(time);
            }

            // set up the RecyclerView
            RecyclerView recyclerView = findViewById(R.id.rclTimes);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            timesAdapter = new TimesAdapter(this, timesList);
            //timesAdapter.setClickListener((TimesAdapter.ItemClickListener) this);
            recyclerView.setAdapter(timesAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean showReturn() {
        LocalTime noon = LocalTime.parse("12:00");
        LocalTime now = LocalTime.now(ZoneId.systemDefault());

        if (now.isBefore(noon)) { // Both are on the same day
            return false;
        } else { // end is on the next day
            return true;
        }
    }

    private void update(String[] stationIds){
        String payload = "{\"nucleo\":\"10\",\"origen\":\"##origen##\",\"destino\":\"##destino##\",\"fchaViaje\":\"##fecha##\",\"validaReglaNegocio\":true,\"tiempoReal\":true,\"servicioHorarios\":\"VTI\",\"horaViajeOrigen\":\"##horaSalida##\",\"horaViajeLlegada\":\"24\",\"accesibilidadTrenes\":true}";

        String currDate = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
        String currTime = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());

        payload = payload.replace("##origen##",stationIds[spFrom.getSelectedItemPosition()]);
        payload = payload.replace("##destino##",stationIds[spTo.getSelectedItemPosition()]);
        payload = payload.replace("##fecha##",currDate);
        payload = payload.replace("##horaSalida##",currTime);

        updateTimes(payload);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spFrom = (Spinner) findViewById(R.id.spFrom);
        spTo = (Spinner) findViewById(R.id.spTo);
        btnSwitch = (Button) findViewById(R.id.btnSwitch);

        final String[] stationIds = getResources().getStringArray(R.array.stations_ids);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.stations_names, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spFrom.setAdapter(adapter);
        spTo.setAdapter(adapter);

        if(showReturn()){
            spFrom.setSelection(cham);
            spTo.setSelection(valde);
        }else{
            spFrom.setSelection(valde);
            spTo.setSelection(cham);
        }

        spFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int pos, long id) {
                update(stationIds);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int pos, long id) {
                update(stationIds);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        btnSwitch.setOnClickListener(new AdapterView.OnClickListener(){
            @Override
            public void onClick(View v) {
                int tmp = spTo.getSelectedItemPosition();
                spTo.setSelection(spFrom.getSelectedItemPosition());
                spFrom.setSelection(tmp);
            }
        });
    }
}
Currently only supports Madrid Cercanías trains.

It's essentially a UI for something like this:

``` bash
curl --header "Content-Type: application/json" --request POST \
    --data '{
    "nucleo":"10",
    "origen":"19002",
    "destino":"17000",
    "fchaViaje":"20200304",
    "validaReglaNegocio":true,
    "tiempoReal":true,
    "servicioHorarios":"VTI",
    "horaViajeOrigen":"10",
    "horaViajeLlegada":"13",
    "accesibilidadTrenes":true}' \
    https://horarios.renfe.com/cer/HorariosServlet
```

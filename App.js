import 'react-native-gesture-handler';
import React, {useEffect, useState, useRef} from 'react';
import { TouchableOpacity, Text, View, StyleSheet, StatusBar } from 'react-native';
import { AutocompleteDropdown } from 'react-native-autocomplete-dropdown'
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useColorScheme } from 'react-native';
import { useTheme } from '@react-navigation/native';

import TimeTable from './modules/TimeTable';
import StationList from './modules/StationList';
import DPick from './modules/DPick';
import TPick from './modules/TPick';

const storeData = async (key, value) => {
  try {
    const jsonValue = JSON.stringify(value)
    console.log("writing "+jsonValue+" to "+key);
    await AsyncStorage.setItem('@'+key, jsonValue)
  } catch (e) {
      console.log("error" + e)
  }
}

const getData = async (key) => {
  try {
    let jsonValue = await AsyncStorage.getItem('@'+key)
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch(e) {
      console.log("error" + e)
  }
}

const HomeScreen = ({ navigation }) => {
  const [from, setFrom] = useState(null);
  const [to, setTo] = useState(null);
  const [date, setDate] = useState(new Date());

  const stationListFrom = useRef();
  const stationListTo = useRef();

  const datePickerController = useRef();

  const { colors } = useTheme();

  useEffect(() => {
    getData("defaultTime").then((time) => {

      time = new Date(time);

      const currTime = new Date();
      currTime.setFullYear(1970,0,1);

      const swap = true ? currTime > time : false;

      const fromKey = swap ? "defaultTo" : "defaultFrom";
      const toKey = swap ? "defaultFrom" : "defaultTo";

      getData(fromKey).then((data) => {
          setFrom(data)
          stationListFrom.current.stationListController.current.setItem(data);
      });
      getData(toKey).then((data) => {
          setTo(data)
          stationListTo.current.stationListController.current.setItem(data);
      });
    });
  }, []);

  const getFrom = (item) => {
    setFrom(item);
    console.log("from",item);
  }

  const getTo = (item) => {
    setTo(item);
    console.log("to",item);
  }

  const getDate = (d) => {
    setDate(d);
    console.log("date",d);
  }

  const resetDate = () => {
    let currDate = new Date();
    currDate.setMinutes(0);
    console.log(currDate);
    setDate(currDate);
    datePickerController.current.setDate(currDate);
  }

  const switchStations = () => {
    const oldto = to;
    setTo(from);
    setFrom(oldto);
    stationListFrom.current.stationListController.current.setItem(oldto);
    stationListTo.current.stationListController.current.setItem(from);
  }

  return (
    <View style={styles.container}>
      <StatusBar hidden = {false} translucent = {true}/>
      <DPick ref={datePickerController} getDate={getDate} />
      <View style={{flexDirection:"row"}}>
        <View style={{flex:9}}>
          <StationList ref={stationListFrom} getStation={getFrom} placeholder={"From:"} />
          <StationList ref={stationListTo} getStation={getTo} placeholder={"To:"} />
        </View>
        <View style={{flex:1, marginTop:25, marginLeft:15}} >
          <TouchableOpacity onPress={() => switchStations()}>
            <Icon name="arrow-up" size={20} color={colors.text} />
            <Icon name="arrow-down" size={20} color={colors.text} />
          </TouchableOpacity>
        </View>
      </View>
      {from && to ?  <TimeTable origen={from.id} destino={to.id} fecha={date} resetDate={resetDate} /> : <Text>Please select "from" and "to" stations</Text>}
    </View>
  );
}

const PreferencesScreen = ({ navigation }) => {
  const [from, setFrom] = useState(null);
  const [to, setTo] = useState(null);
  const [time, setTime] = useState(new Date());

  const stationListFrom = useRef();
  const stationListTo = useRef();
  const timePickerController = useRef();

  useEffect(() => {
    getData("defaultFrom").then((data) => {
        setFrom(data);
        stationListFrom.current.stationListController.current.setItem(data);
    });
    getData("defaultTo").then((data) => {
        setTo(data)
        stationListTo.current.stationListController.current.setItem(data);
    });
    getData("defaultTime").then((data) => {
        data = data ? new Date(data) : new Date("1970-01-01T12:00:00.000Z");
        setTime(data);
        timePickerController.current.setDate(data);
    });
  }, []);

  const getFrom = (item) => {
    setFrom(item);
  }

  const getTo = (item) => {
    setTo(item);
  }

  const getTime = (time) => {
    setTime(time);
  }

  const firstFrom = useRef(true);
  const firstTo = useRef(true);
  const firstTime = useRef(true);

  useEffect (() => {
    if(firstFrom.current){
      firstFrom.current = false;
      return
    }
    storeData('defaultFrom',from);
  },[from]);

  useEffect (() => {
    if(firstTo.current){
      firstTo.current = false;
      return
    }
    storeData('defaultTo',to);
  },[to]);

  useEffect (() => {
    if(firstTime.current){
      firstTime.current = false;
      return
    }
    storeData('defaultTime',time);
  },[time]);

  return (
    <View style={styles.container}>
      <StationList ref={stationListFrom} getStation={getFrom} placeholder={"Default from:"} />
      <StationList ref={stationListTo} getStation={getTo} placeholder={"Default to:"} />
      <TPick ref={timePickerController} getTime={getTime} />
    </View>
  );
}

const Drawer = createDrawerNavigator();

const App = () => {

  const scheme = useColorScheme();

  return (
     <NavigationContainer theme={scheme === 'dark' ? DarkTheme : DefaultTheme}>
      <Drawer.Navigator initialRouteName="Home" screenOptions={{headerTintColor: scheme == 'dark' ? 'lightgray' : 'black'}}>
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="Preferences" component={PreferencesScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:null
  },
});

export default App;
